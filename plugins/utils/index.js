'use strict'
/**
 * 解析歌词，得到一个歌词对象数组
 * 每个歌词对象格式 
 * {time: 开始时间；words: 歌词内容}
 */
export function parseLrc(lrc) {
    var lines = lrc.split('\n');
    var result = []
    for (var i = 0; i < lines.length; i++) {
        var str = lines[i].split("]")
        var timeStr = str[0].substring(1)
        var obj = {
            time: parseTime(timeStr),
            words: str[1]
        }
        result.push(obj)
    }
    return result
}

/**
 * 格式化时间
 * 将时间字符串解析为时间（秒）
 */
export function parseTime(timeStr) {
    var str = timeStr.split(":")
    var time = +str[0] * 60 + +str[1]
    return time
}