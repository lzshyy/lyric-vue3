// 组件
import LyricBasic from './components/lyricBasic'

const components = [LyricBasic]

// 定义install方法，Vue作为参数
const install = Vue => {
    // 判断是否被install过
    if (install.installed) return
    install.installed = true
    // 遍历所有组件
    components.map(component => Vue.component(component.name, component)
    )
}

if (typeof window !== "undefined" && window.Vue) {
    install(window.Vue)
}

export default {
    install,
    // 所有组件，必须具有install方法才能使用Vue.use()
    ...components
}