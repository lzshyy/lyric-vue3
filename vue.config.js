const { defineConfig } = require('@vue/cli-service')
const path = require('path')

module.exports = defineConfig({
  transpileDependencies: true,
  css: {
    extract: false, // 是否将组件中的 CSS 提取至一个独立的 CSS 文件中 (而不是动态注入到 JavaScript 中的 inline 代码)。
  },
  // 简单方式
  configureWebpack: config => {
  },
  // 链式操作
  chainWebpack: config => {
    // @ 默认指向 src 目录
    // 新增一个 ~ 指向 plugins
    // config.resolve.alias
    //   .set('~', path.resolve('plugins'))

    // 把 plugins 加入编译，因为新增的文件默认是不被 webpack 处理的
    // config.module
    //   .rule('js')
    //   .include.add(/plugins/).end()
    //   .use('babel')
    //   .loader('babel-loader')
    //   .tap(options => {
    //     // 修改它的选项...
    //     return options
    //   })
  }
})
