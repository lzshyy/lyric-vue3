# lyric-vue3

## 介绍
```
功能：vue3下的歌词滚动插件，播放时间与歌词联动滚动，可自定义样式
```

### 使用
```
npm install lyric-vue3
```

```
import lyric from 'lyric-vue3'
createApp(App).use(lyric).mount('#app')
```
```
<LyricBasic :config="config"></LyricBasic>
```

## config
```
{
    src: String, // （必填）音频文件地址，支持Ogg、MP3和WAV
    lyrStr: String, // （必填）歌词字符串，格式严格按照lrc标准格式
    scorllSpeed: Number, // （默认0.6）歌词自动滚动速度
    scaleSpeed: Number, // （默认0.3）歌词缩放速度
    scalePercent: Number, // （默认1.2）歌词缩放比例
}
```

## example
```
<template>
  <LyricBasic :config="config" class="lyr"></LyricBasic>
</template>

<script setup>
import { reactive } from 'vue';
import lrc from './assets/music'

const config = reactive({
  src: require('./assets/music.mp3'),
  lyrStr: lrc,
  scorllSpeed: 0.6,
  scaleSpeed: 0.3,
  scalePercent: 1.2
})

</script>

<style scoped>
/**
 * 通过样式穿透修改样式
 * 1、css 用 <<< 
 * 2、scss 用 /deep/ ,vue-cli3以上版本用 ::v-deep
 */
.lyr >>>.container .active {
  color: aqua;
}
</style>
```
